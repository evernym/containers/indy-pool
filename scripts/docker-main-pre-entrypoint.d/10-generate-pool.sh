#!/bin/bash
CONTAINER_IP=$(ip addr | grep 'inet ' | grep -v 'host lo' | awk '{print $2}' | cut -d '/' -f 1 | head -n 1)
POOL_IP=${POOL_IP:-$CONTAINER_IP}
LEDGER_NAME=${LEDGER_NAME:-sandbox}

if ! grep -q 'NETWORK_NAME' /etc/indy/indy_config.py; then
        echo "NETWORK_NAME='${LEDGER_NAME}'" >> /etc/indy/indy_config.py
else
    sed -i "s/^NETWORK_NAME.*/NETWORK_NAME='${LEDGER_NAME}'/" /etc/indy/indy_config.py
fi

if [ -f "/var/lib/indy/${LEDGER_NAME}/pool_transactions_genesis" ] ; then
    if ! grep -q "${POOL_IP//\./\\.}" /var/lib/indy/${LEDGER_NAME}/pool_transactions_genesis; then
        echo "IP appears to have changed. Resetting ledger...."
        if [ -d /var/lib/indy/$LEDGER_NAME ] ; then
            rm -rf "/var/lib/indy/${LEDGER_NAME}"
        fi
    fi
fi

if [ ! -d /var/lib/indy/$LEDGER_NAME ] ; then
    echo "Generating genesis transaction file using IP: ${POOL_IP}"
    chown indy /var/lib/indy
    echo "Creating New Ledger, with name: ${LEDGER_NAME}"
    su - indy -s /bin/bash -c "
    generate_indy_pool_transactions --network ${LEDGER_NAME} --nodes 4 --clients 5 --nodeNum 1 2 3 4 --ips '${POOL_IP},${POOL_IP},${POOL_IP},${POOL_IP}'
    "
fi
