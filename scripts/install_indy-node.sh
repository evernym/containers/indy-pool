#!/bin/bash

## Helper Functions ##
function gen_pkgs_to_install {
    local package=$1
    local pkgs=''
    local ps=( $(apt-cache show $package | grep Depends | head -n 1 | sed 's/^Depends:// ; s/([<>]=[^()]\+)/ /g; s/([<>]\{1,2\}[^()]\+)//g ; s/[() ]//g; s/,/ /g') )
    local pstr="$package"
    local p
    local dps
    for p in "${ps[@]}"; do
        if [[ $p =~ '=' ]]; then
            pstr="$pstr\n$p"
            dps=$(gen_pkgs_to_install $p)
            echo "package: $package needs: ${dps//$'\n'/,}" >&2
            pstr="$pstr\n$dps"
        fi
    done
    echo -e "$pstr" | sort -u
}

## Main Flow ##
#This grabs all of the dependent packages and their explicit versions
node_version="${INDY_NODE}"
if [ ! -z "$node_version" ] ; then
    node_version="=$node_version"
fi
libsovtoken_version="${LIBSOVTOKEN}"
if [ ! -z "$libsovtoken_version" ] ; then
    libsovtoken_version="=$libsovtoken_version"
fi

apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 97080EBDA5D46DAF
echo "deb https://repo.sovrin.org/deb xenial stable" >> /etc/apt/sources.list.d/sovrin.list
apt-get update
echo "Generating list of explicitly defined versions"
packages=$(gen_pkgs_to_install indy-node${node_version} 2>/dev/null)
echo "$packages"
apt-get install --no-install-recommends -y $packages supervisor || exit 1

adduser --system --group --home /home/indy --shell /bin/bash indy

if [ ! -z "$SOVTOKEN" ] ; then
    echo "Installing sovtoken packages"
    /etc/ledger_helpers/sovtoken_manage.sh --setup-sovtoken-only --sovtoken-vers "$SOVTOKEN" --sovtokenfees-vers "$SOVTOKENFEES" || exit 1
fi
