#!/bin/bash

set -e

# run any custom pre-scripts
if [ -d /docker-pre-entrypoint.d/ ]; then
    for script in $(find /docker-pre-entrypoint.d/ -type f -name "*.sh" | sort -n); do
        chmod +x "$script"
        sync
        echo -e "\nRunning Custom Pre-script: '$script'"
        "$script"
        rc=$?
        if [ $rc -ne 0 ] ; then
            exit $rc
        fi
    done
fi

for script in $(find /docker-main-pre-entrypoint.d/ -type f -name "*.sh" | sort -n); do
    chmod +x "$script"
    sync
    echo -e "\nRunning Main Pre-script: '$script'"
    "$script"
    rc=$?
    if [ $rc -ne 0 ] ; then
        exit $rc
    fi
done

echo -e "\nStarting Indy Nodes"
/usr/bin/supervisord -c /etc/supervisord.conf &
sleep 1 # TODO we should have a better way to wait for the ledger pool to be up


for script in $(find /docker-main-post-entrypoint.d/ -type f -name "*.sh" | sort -n); do
    chmod +x "$script"
    sync
    echo -e "\nRunning Main Post-script: '$script'"
    "$script"
    rc=$?
    if [ $rc -ne 0 ] ; then
        exit $rc
    fi
done

# run any custom post-scripts
if [ -d /docker-post-entrypoint.d/ ]; then
    for script in $(find /docker-post-entrypoint.d/ -type f -name "*.sh" | sort -n); do
        chmod +x "$script"
        sync
        echo -e "\nRunning Custom Post: '$script'"
        "$script"
        rc=$?
        if [ $rc -ne 0 ] ; then
            exit $rc
        fi
    done
fi

echo -e "\nStartup Actions Completed"
# Wait back on the supervisord process
wait
