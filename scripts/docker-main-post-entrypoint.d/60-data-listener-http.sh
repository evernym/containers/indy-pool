#!/bin/bash

source /etc/ledger_helpers/env
gen_file=/var/lib/indy/${LEDGER_NAME}/pool_transactions_genesis
server_dir=/var/lib/http_genesis/genesis

mkdir -p "${server_dir}"
cd "${server_dir}" || exit
cp "${gen_file}" "${server_dir}"/genesis.txt

python3 -m http.server 5679 &