#!/bin/bash

source /etc/ledger_helpers/env
if [ "$FEES_ENABLE" == true ] ; then
    if [ "$MINT_TOKENS" == true ] ; then
        if [ ! -f "/var/lib/indy/${LEDGER_NAME}/tokens_have_been_minted" ] ; then
            ##Mint Tokens
            echo "Minting: '${MINT_TOKENS_AMOUNT}' to address: '${MINT_ADDRESS}'"
            WALLET_KEY=d3vl4b /etc/ledger_helpers/ledger.sh --env-file /etc/ledger_helpers/env --mint --genesis-file /var/lib/indy/${LEDGER_NAME}/pool_transactions_genesis
            mt_rc=$?
            if [ $mt_rc -ne 0 ] ; then
                echo "ERRORS minting tokens on the ledger" >&2
                exit 1
            else
                touch "/var/lib/indy/${LEDGER_NAME}/tokens_have_been_minted"
            fi
        fi
    else
        echo "Skipping minting of tokens on the ledger, as minting is disabled"
    fi
else
    echo "Skipping the minting of tokens on the ledger as fees are disabled"
fi
