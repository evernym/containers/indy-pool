#!/bin/bash

source /etc/ledger_helpers/env
if [ "$FEES_ENABLE" == true ] ; then
    if [ ! -f "/var/lib/indy/${LEDGER_NAME}/fees_have_been_set" ] ; then
        ##Set fees
        WALLET_KEY=d3vl4b /etc/ledger_helpers/ledger.sh --env-file /etc/ledger_helpers/env --set-fees
        sf_rc=$?
        if [ $sf_rc -ne 0 ] ; then
            echo "ERRORS setting fees on the ledger" >&2
            exit 1
        else
            touch "/var/lib/indy/${LEDGER_NAME}/fees_have_been_set"
        fi
    fi
else
    echo "Skipping the setting of ledger fees, as they aren't enabled"
fi
