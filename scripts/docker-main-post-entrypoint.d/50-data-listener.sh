#!/bin/bash

function serve {
    source /etc/ledger_helpers/env
    while true ; do
        nc -v -n -l -p 5678 < /var/lib/indy/${LEDGER_NAME}/pool_transactions_genesis
    done
}

serve &
