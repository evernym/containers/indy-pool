#!/bin/bash

source /etc/ledger_helpers/env
if [ "$TAA_ENABLE" == true ] ; then
    if [ ! -f "/var/lib/indy/${LEDGER_NAME}/taa_has_been_set" ] ; then
        ##Set TAA
        echo "Setting a TAA on the ledger"
        /etc/ledger_helpers/ledger.sh --env-file /etc/ledger_helpers/env --set-taa "TAA for ${LEDGER_NAME} ledger"
        taa_rc=$?
        if [ $taa_rc -ne 0 ] ; then
            echo "ERRORS setting the Transaction Auth Agreement on the ledger" >&2
            exit 1
        else
            touch "/var/lib/indy/${LEDGER_NAME}/taa_has_been_set"
        fi
    fi
else
    echo "Skipping the setting of a Transaction Auth Agreement (TAA), as it is disabled"
fi
