#!/bin/bash

source /etc/ledger_helpers/env
if [ "$FEES_ENABLE" == "true" -o "$MINT_TOKENS" == "true" -o "$TAA_ENABLE" == "true" ]; then
    if [ ! -f "/var/lib/indy/${LEDGER_NAME}/dids_have_been_preseeded" ] ; then
        /etc/ledger_helpers/ledger.sh --seed-dids --genesis-file /var/lib/indy/${LEDGER_NAME}/pool_transactions_genesis
        if [ $? -ne 0 ] ; then
            echo "Errors occurred trying to preseed dids" >&2
            exit 1
        fi
        touch "/var/lib/indy/${LEDGER_NAME}/dids_have_been_preseeded"
    fi
else
    echo "Skipping did preseeding to ledger as no fees are enable, no taa is to be enabled, and neither is token minting"
fi
