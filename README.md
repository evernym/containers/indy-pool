# This repo hosts Indy-Pool Images

NOTE: The pool will auto start its nodes on the `9701-9708` ports and uses the IP Address that the docker container is hosted on.

**If you want the pool to use a custom or localhost ip then run the image with `BIND_TO_HOST=true` and `HOST_IP={ip_to_use}`**

## The tag of the image will correspond to the indy and sovtoken versions used in the pool

`[indy-node-version]_[sovtoken_version]`

For example: 
* 1.10.0_1.0.3

## Images are stored in

* [Packages -> Container Registry -> dev/containers/indy-pool](https://gitlab.corp.evernym.com/dev/containers/indy-pool/container_registry)

* Only the master branch will upload images, if you want it to store your own custom images in a container repo fork this and make changes on the master branch there.

You should be able to easily pull them anywhere that you can access the gitlab from, using:

`docker pull gitlab.corp.evernym.com:4567/dev/containers/indy-pool:{image_tag}`

### If the version you are looking for doesnt exist, create your own using steps below

## Creating a new image

Go to [CI / CD](https://gitlab.corp.evernym.com/dev/containers/indy-pool/pipelines) and click `Run Pipeline`

* **If sovtoken_version isn't set, it will not be included in the pool.**
* **If other variables are not set, versions will default to the latest.**

## Building locally

If you would like to build the image locally, you just need to set the appropriate `build-args` in your `docker build` command.

Build Args

| Arg | Description |
| --- | ---         |
| indy_node | The version of `indy-node` to install in the image |
| sovtoken | The version of `sovtoken` to install in the image |
| sovtokenfees | The version of `sovtokenfees` to install in the image |

Example:

```
docker build --build-arg indy_node=1.10.0 --build-arg sovtoken=1.0.3 --build-arg sovtokenfees=1.0.3 -t local-indy-pool:latest ./
```

### Currently supported version variables

* `INDY_NODE_VERSION`
* `SOVTOKEN_VERSION`
* `SOVTOKENFEES_VERSION`
  * NOTE: If this is left blank it will default to the value of `SOVTOKEN_VERSION`

The following environment variables change what repository to look at for determining the latest available version

| Variable | Default  | Description |
| ---      |  ---     | ---         |
| `REPO_URL` | https://repo.sovrin.org/deb | The location of the repository |
| `REPO_DIST` | xenial | Which `dist` inside the repo to look at |
| `REPO_COMP` | stable | Which component of the repo to use |


If `INDY_NODE_VERSION` and/or `SOVTOKEN_VERSION` is used, then the latest version from the repository will be queried and used.

### Set versions for a single run

Go to [CI / CD](https://gitlab.corp.evernym.com/dev/containers/indy-pool/pipelines) and click `Run Pipeline`, it will prompt you for the variables.

### To set explicit versions of indy-node and sovtoken for all future builds

Inside Gitlab go to [Settings / CI/CD](https://gitlab.corp.evernym.com/dev/containers/indy-pool/-/settings/ci_cd), and under `Variables` set the variables.

The `indy-node` and `sovtoken` packages generally follow standard semver conventions

**Once Variables are set go to [CI / CD](https://gitlab.corp.evernym.com/dev/containers/indy-pool/pipelines) and click `Run Pipeline`**

## Using the image

This image has some "magic" built in to make it very flexible for using.

### Retrieving genesis file for CI/CD via raw TCP

The image by default exposes port `5678` and when connected to a tcp client it will send it's genesis file, 
this allows an easy way to access the file even when not running on the same localhost.

For example, the following code would connect with netcat and save the genesis file to `path/and/file_name.txt`:

* `nc -v -w 2 ${POOL_IP} 5678 > [path/and/file_name.txt]`

### Retrieving genesis file for CI/CD via HTTP

The image by default exposes simple web-server on port `5679` This web server allows an easy way to access the file even when not running on the same localhost. 

The url is: `http://${POOL_IP}:5679/genesis.txt`

For example, the following code would connect with curl and save the genesis file to `path/and/file_name.txt`:

* `curl http://${POOL_IP}:5679/genesis.txt > [path/and/file_name.txt]`


### Environment Variables

| Env. Variable      | Default   | Description |
| ---                | ---       | ---         |
| FEES_ENABLE        | `false`   | Set up fees on the ledger. Value can be `true` or `false`|
| FEES               | `1:0,100:0,101:0,102:0,10001:0` | Set the value of the fees. Value is a semi-colon separate list of fees. This has no effect if `FEES_ENABLE` is `false` |
| LEDGER_NAME        | `sandbox` | Specifies an alternate name of the ledger to use when the pool is initialized on startup |
| MINT_TOKENS        | `false`   | Mint some tokens on the ledger on startup. Value can be `true` or `false` |
| MINT_TOKENS_AMOUNT | `1000`    | Set the amount of tokens to mint. This has no effect if `MINT_TOKENS` is `false` |
| MINT_ADDRESS       | `pay:sov:2VDx3iWWSJusBmop3HH5A4VdK6UyYwHoT2zGXTBLoJskxuGVhP` | Specify the payment address where the minted tokens should be placed. The seed for the default address is `000000000000000000000000Address1` |
| POOL_IP            | IP of the container | Specifies the IP address for the nodes to communicate on and use |
| TAA_ENABLE         | `false`   | Enable a Transaction Auth Agreement on the ledger on startup. Value can be `true` or `false |

#### Environmet Variable Examples

Example To enable fees and set them to 0:

```
docker run --rm -it --env FEES_ENABLE=true -p 9701-9708 <image:tag>
```

Example To enable fees and set them to transaction type of `1` to have a fee of `5` tokens:

```
docker run --rm -it --env FEES_ENABLE=true --env FEES=1:5 -p 9701-9708 <image:tag>
```

Example of setting a custom ledger name, using 127.0.0.1:

```
docker run --rm -it --env POOL_IP=127.0.0.1 --env LEDGER_NAME=my_test -p 9701-9708 <image:tag>
```

Example to mint tokens using the default payment address and the default number of tokens `1000`:

```
docker run --rm -it --env MINT_TOKENS=true -p 9701-9708 <image:tag>
```

Example to mint `2000` tokens to the default payment address:

```
docker run --rm -it --env MINT_TOKENS=true --env MINT_TOKENS_AMOUNT=2000 -p 9701-9708 <image:tag>
```

Example to mint `2000` tokens to the a custom payment address:

```
docker run --rm -it --env MINT_TOKENS=true --env MINT_TOKENS_AMOUNT=2000 --env MINT_ADDRESS=pay:sov:kZkyNDbxia5cmmeyT88uhhC1aKRwU15Ew5LkfwABsSqv56Sxy -p 9701-9708 <image:tag>
```

Example:

```
docker run --rm -it --env POOL_IP=127.0.0.1 -p 9701-9708 <image:tag>
```

Example to enable a TAA:

```
docker run --rm -it --env TAA_ENABLE=true -p 9701-9708 <image:tag>
```

### Volumes

| Volume | Description |
| ---    | ---         |
| /docker-pre-entrypoint.d | Custom scripts that are to be run BEFORE the ledger is generated and started |
| /docker-post-entrypoint.d | Custom script that are to be run AFTER the ledger has been started |
| /var/lib/indy | Location for the ledger's data files |

### Custom Scripts

The `entrypoint` into the container is found inside the container at: `/usr/bin/indy-node-startup.sh` This script does the following actions, in this order:

1. Run any ***custom*** scripts found in `/docker-pre-entrypoint.d`
1. Run main pre scripts found in `/docker-main-pre-entrypoint.d` (Used primarily for creating a ledger etc..)
1. Execute supervisord to start up the indy-node processes
1. Run main scripts found in `/docker-main-post-entrypoint.d` (Used for actions that require the ledger to already be started, like setting fees etc...)
1. Run any ***custom*** scripts found in `/docker-post-entrypoint.d`

So if you needed to run some scripts before the genesis transaction file is generated then create the files, then volume mount them (`-v` option in `docker run`) to `/docker-pre-entrypoint.d` inside the container. They will be executed in alpha numerical order.

Example:
```
docker run -v $PWD/pre-scripts.d:/docker-pre-entrypoint.d <image>
```
