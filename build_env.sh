#!/bin/bash

REPO_URL="${REPO_URL:-https://repo.sovrin.org/deb}"
REPO_DIST="${REPO_DIST:-xenial}"
REPO_COMP="${REPO_COMP:-stable}"
REPO_ARCH="${REPO_ARCH:-amd64}"

function install_prereqs {
    apt-get update &&\
    apt-get install -y --no-install-recommends curl ca-certificates
}

function get_packages_url {
    local repo_url="$1"
    local dist="${2:-xenial}"
    local comp="${3:-stable}"
    local arch="${4:-amd64}"
    local comp_url=""
    local release_file=""
    local packages_url=""

    #Get the Release file:
    release_file=$(curl -Ss ${repo_url}/dists/${dist}/Release 2>&1)
    rc=$?
    if [ $rc -ne 0 ] ; then
        echo "$release_file" >&2
        echo "Failed to get Release file from: ${repo_url}/dists/${dist}/Release" >&2
        return 1
    fi
    #Get the component destination
    comp_url=$(echo "$release_file" | grep "${comp}/[^/]\+/Packages\$" | grep -E "$arch|all" | cut -d ' ' -f 4 | sort -u | head -n 1)
    if [ -z "$comp_url" ] ; then
        echo "Failed to parse release file from: '${repo_url}/dists/${dist}/Release' to get the url of component: '$comp'" >&2
        return 1
    fi
    #Generate the final component Packages destination
    packages_url="${repo_url}/dists/${dist}/${comp_url}"
    echo "$packages_url"
}

function get_pkg_vers_repo {
    local pkg="$1"
    pkgs_url=$(get_packages_url $REPO_URL $REPO_DIST $REPO_COMP $REPO_ARCH)
    rc=$?
    if [ $rc -ne 0 ] ; then
        return $rc
    fi
    versions=$(curl -Ss "$pkgs_url"  | grep -A 1 "^Package: ${pkg}" | grep Version | cut -d ' ' -f 2 | sort -V)
    if [ -z "$versions" ] ; then
        echo "Package: '${pkg}' not found" >&2
        return 1
    fi
    echo "$versions"
}

if [ -z "$INDY_NODE_VERSION" ] ; then
    install_pq=true
elif [ -z "$SOVTOKEN_VERSION" ] ; then
    install_pq=true
fi

if [ "$install_pq" == true ] ; then
    install_prereqs
fi

if [ -z "$INDY_NODE_VERSION" -o "$INDY_NODE_VERSION" == 'latest' ] ; then
    VERSIONS=$(get_pkg_vers_repo indy-node)
    rc=$?
    if [ $? -ne 0 ] ; then
        exit 1
    fi
    INDY_NODE_VERSION=$(echo "$VERSIONS" | tail -n 1)
fi
if [ -z "$SOVTOKEN_VERSION" -o "$SOVTOKEN_VERSION" == 'latest'  ] ; then
    VERSIONS=$(get_pkg_vers_repo sovtoken)
    rc=$?
    if [ $? -ne 0 ] ; then
        exit 1
    fi
    SOVTOKEN_VERSION=$(echo "$VERSIONS" | tail -n 1)
fi
SOVTOKENFEES_VERSION="${SOVTOKENFEES_VERSION:-$SOVTOKEN_VERSION}"

PDIR=$(dirname "$1")
mkdir -p "$PDIR"

cat <<EOF > "${PDIR}/env"
export INDY_NODE_VERSION="$INDY_NODE_VERSION"
export SOVTOKEN_VERSION="$SOVTOKEN_VERSION"
export SOVTOKENFEES_VERSION="$SOVTOKENFEES_VERSION"
EOF
