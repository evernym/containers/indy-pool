FROM ubuntu:16.04

ARG indy_node
ARG sovtoken
ARG sovtokenfees

ENV INDY_NODE=$indy_node
ENV SOVTOKEN=$sovtoken
ENV SOVTOKENFEES=$sovtokenfees

EXPOSE 9701 9702 9703 9704 9705 9706 9707 9708 5678 5679

# Install apt pre-reqs
RUN apt-get update && \
    apt-get install -y --no-install-recommends apt-transport-https ca-certificates iproute2 procps netcat && \
    rm -rf /var/lib/apt/lists/*

#Copy over supervisord config
COPY files/indy-supervisord /etc/supervisord.conf

# Install indy-node
COPY scripts/install_indy-node.sh /etc/ledger_helpers/
COPY scripts/sovtoken_manage.sh /etc/ledger_helpers/
COPY scripts/ledger.sh /etc/ledger_helpers/
RUN /etc/ledger_helpers/install_indy-node.sh && \
    rm -rf /var/lib/apt/lists/*

#Copy our startup script pieces:
COPY scripts/docker-main-pre-entrypoint.d /docker-main-pre-entrypoint.d
COPY scripts/docker-main-post-entrypoint.d /docker-main-post-entrypoint.d

#Copy our initialization script:
COPY scripts/indy-node-startup.sh /usr/bin/indy-node-startup.sh

VOLUME ["/docker-pre-entrypoint.d", "/docker-post-entrypoint.d", "/var/lib/indy"]

ENTRYPOINT [ "/usr/bin/indy-node-startup.sh" ]
